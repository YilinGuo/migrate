# -*- coding: utf-8 -*-
import time
import boto3
from boto3 import Session
import pandas as pd
import json
from requests_aws4auth import AWS4Auth
from elasticsearch import Elasticsearch, RequestsHttpConnection

# query = "WITH events_ordered_table AS ( SELECT attributes.userId AS userId, session.session_id AS sessionId, event_timestamp, from_unixtime(event_timestamp/1000) AS tmp_timestamp, CAST(ROW(event_type, event_timestamp, arrival_timestamp, event_version, application, client, device, session, attributes, metrics, endpoint, awsaccountid, year, month, day, hour) AS ROW(event_type VARCHAR, event_timestamp BIGINT, arrival_timestamp BIGINT, event_version VARCHAR, application ROW(app_id VARCHAR, cognito_identity_pool_id VARCHAR, package_name VARCHAR, sdk ROW(name VARCHAR, version VARCHAR), title VARCHAR, version_name VARCHAR, version_code VARCHAR), client ROW (client_id VARCHAR, cognito_id VARCHAR), device ROW(locale ROW(code VARCHAR, country VARCHAR, language VARCHAR), make VARCHAR, model VARCHAR, platform ROW(name VARCHAR, version VARCHAR)), session ROW(session_id VARCHAR, start_timestamp BIGINT, stop_timestamp BIGINT), attributes ROW(source VARCHAR, userid VARCHAR, ingressname VARCHAR, networkcall VARCHAR, region VARCHAR, calltype VARCHAR, pagename VARCHAR, impressionblog VARCHAR, feedname VARCHAR, error VARCHAR, clickblog VARCHAR, blogid VARCHAR, highvalueaction VARCHAR, likeblog VARCHAR, unlikeblog VARCHAR, apiname VARCHAR, bottomtabitem VARCHAR, featurename VARCHAR, action VARCHAR, retrycount VARCHAR, timeoutduration VARCHAR, response_name VARCHAR, url VARCHAR), metrics ROW(impressioncount INT, clickcount INT, millisecond INT, timespend INT, commentsloadedcount INT, viewedimagecount INT, allcommentsloaded INT, totalimagecount INT, timeoutduration INT), endpoint ROW(channeltype VARCHAR, endpointstatus VARCHAR, optout VARCHAR, requestid VARCHAR, location ROW(country VARCHAR, postalcode VARCHAR, city VARCHAR, region VARCHAR), demographic ROW(make VARCHAR , model VARCHAR, timezone VARCHAR, locale VARCHAR, appversion VARCHAR, platform VARCHAR, platformversion VARCHAR), effectivedate VARCHAR, attributes VARCHAR , metrics VARCHAR , applicationid VARCHAR, id VARCHAR, cohortid VARCHAR, creationdate VARCHAR, user ROW(userattributes VARCHAR)), awsaccountid VARCHAR, year VARCHAR, month VARCHAR, day VARCHAR, hour VARCHAR)) AS event FROM pinpointstream WHERE attributes.userId IS NOT NULL AND (from_unixtime(event_timestamp/1000) > (current_timestamp - interval '10' HOUR)) ORDER BY  from_unixtime(event_timestamp)) SELECT userId, from_unixtime(min(event_timestamp)/1000) AS list_timestamp, array_agg(event) AS events FROM (events_ordered_table) GROUP BY userId, sessionId ORDER BY min(event_timestamp);"
query = "WITH events_ordered_table AS (SELECT coalesce(attributes.userId, client.client_id) AS userId, session.session_id AS sessionId, event_timestamp, from_unixtime(event_timestamp/1000) AS tmp_timestamp, event_type, application.version_name AS app_version_name, application.version_code AS app_version_code, client.client_id AS client_client_id, client.cognito_id AS client_cognito_id, device.locale.code AS device_locale_code, device.locale.country AS device_locale_country, device.locale.language AS device_locale_language, device.make AS device_make, device.model AS device_model, device.platform.name AS device_platform_name, device.platform.version AS device_platform_version, session.start_timestamp AS session_start_timestamp, session.stop_timestamp AS session_stop_timestamp, attributes, metrics, endpoint.channeltype AS endpoint_channeltype, endpoint.endpointstatus AS endpoint_endpointstatus, endpoint.optout AS endpoint_optout, endpoint.requestid AS endpoint_requestid, endpoint.location.country AS endpoint_location_country, endpoint.location.postalcode AS endpoint_location_postalcode, endpoint.location.city AS endpoint_location_city, endpoint.location.region AS endpoint_location_region, endpoint.demographic.make AS endpoint_demo_make, endpoint.demographic.model AS endpoint_demo_model, endpoint.demographic.timezone AS endpoint_demo_timezone, endpoint.demographic.locale AS endpoint_demo_locale, endpoint.demographic.appversion AS endpoint_demo_appversion, endpoint.demographic.platform AS endpoint_demo_platform, endpoint.demographic.platformversion AS endpoint_demo_platformversion, endpoint.effectivedate AS endpoint_effectivedate, endpoint.attributes AS endpoint_attributes, endpoint.metrics AS endpoint_metrics, endpoint.applicationid AS endpoint_applicationid, endpoint.id AS endpoint_id, endpoint.cohortid AS endpoint_cohortid, endpoint.creationdate AS endpoint_creationdate, endpoint.user.userattributes AS endpoint_user_userattributes FROM pinpointstream WHERE (attributes.userId IS NOT NULL OR client.client_id IS NOT NULL) AND (from_unixtime(event_timestamp/1000) > (current_timestamp - interval '12' HOUR)) ORDER BY  from_unixtime(event_timestamp) ) SELECT userId, sessionId, from_unixtime(min(event_timestamp)/1000) AS list_timestamp, array_agg(event_type) AS event_type_list, array_agg(event_timestamp) AS event_timestamp_list, array_agg(app_version_name) AS app_version_name_list, array_agg(app_version_code) AS app_version_code_list, array_agg(client_client_id) AS client_client_id_list, array_agg(client_cognito_id) AS client_cognito_id_list, array_agg(device_locale_code) AS device_locale_code_list, array_agg(device_locale_country) AS device_locale_country_list, array_agg(device_locale_language) AS device_locale_language_list, array_agg(device_make) AS decice_make_list, array_agg(device_model) AS device_model_list, array_agg(device_platform_name) AS device_platform_name_list, array_agg(device_platform_version) AS device_platform_version_list, array_agg(session_start_timestamp) AS session_start_timestamp_list, array_agg(session_stop_timestamp) AS session_stop_timestamp_list, array_agg(attributes) AS attribute_list, array_agg(metrics) AS metrics_list, array_agg(endpoint_channeltype) AS endpoint_channeltype_list, array_agg(endpoint_endpointstatus) AS endpoint_endpointstatus_list, array_agg(endpoint_optout) AS endpoint_optout_list, array_agg(endpoint_requestid) AS endpoint_requestid_list, array_agg(endpoint_location_country) AS endpoint_location_country_list, array_agg(endpoint_location_postalcode) AS endpoint_location_postalcode_list, array_agg(endpoint_location_city) AS endpoint_location_city_list, array_agg(endpoint_location_region) AS endpoint_location_region_list, array_agg(endpoint_demo_make) AS endpoint_demo_make_list, array_agg(endpoint_demo_model) AS endpoint_demo_model_list, array_agg(endpoint_demo_timezone) AS endpoint_demo_timezone_list, array_agg(endpoint_demo_locale) AS endpoint_demo_locale_list, array_agg(endpoint_demo_appversion) AS endpoint_demo_appversion_list, array_agg(endpoint_demo_platform) AS endpoint_demo_platform_list, array_agg(endpoint_demo_platformversion) AS endpoint_demo_platformversion_list, array_agg(endpoint_effectivedate) AS endpoint_effectivedate_list, array_agg(endpoint_attributes) AS endpoint_attributes_list, array_agg(endpoint_metrics) AS endpoint_metrics_list, array_agg(endpoint_applicationid) AS endpoint_applicationid_list, array_agg(endpoint_id) AS endpoint_id_list, array_agg(endpoint_cohortid) AS endpoint_cohortid_list, array_agg(endpoint_creationdate) AS endpoint_creationdate_list, array_agg(endpoint_user_userattributes) AS endpoint_user_userattributes_list FROM (events_ordered_table) GROUP BY  userId, sessionId ORDER BY  min(event_timestamp);"
DATABASE = 'ergozpinpointeventprod'
CATALOG = 'AwsDataCatalog'
output='s3://pinpointstream-athena-query/'
bucket = 'pinpointstream-athena-query'
REGION_NAME = 'us-west-2'

region = 'us-west-2'
service = 'es'
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(credentials.access_key, credentials.secret_key, region, service, session_token=credentials.token)
EShost = 'https://search-ergoz-appsync-es-beta-esbdykbd44lmyetsvw44lya32e.us-west-2.es.amazonaws.com'
EStableName = "pinpoint-analysis"
ESdocType = "_doc"
ESsearchType = "_search"
ESheaders = {"Content-Type": "application/json"}

session = Session(aws_access_key_id=credentials.access_key, aws_secret_access_key=credentials.secret_key, region_name=REGION_NAME)
s3 = session.resource('s3')
client = boto3.client('athena', aws_access_key_id=credentials.access_key, aws_secret_access_key=credentials.secret_key, region_name=REGION_NAME)
es = Elasticsearch(
            hosts=EShost,
            http_auth=awsauth,
            use_ssl=True,
            verify_certs=True,
            retry_on_timeout=True,
            timeout=60000,
            connection_class=RequestsHttpConnection
        )

def query_athena():
    # Execution, write to S3
    response = client.start_query_execution(
        QueryString=query,
        QueryExecutionContext={
            'Database': DATABASE,
            'Catalog': CATALOG
        },
        ResultConfiguration={
            'OutputLocation': output,
        }
    )
    #read from S3
    fileName = response['QueryExecutionId']
    csvFile = fileName + '.csv'
    metaFile = fileName + '.csv.metadata'
    while (True):
        try:
            obj = s3.Object(bucket, csvFile).get()
            break
        except:
            time.sleep(20) #wait for S3 loading
    initial_df = pd.read_csv(obj['Body'])
    #delete from S3 
    s3.Object(bucket, csvFile).delete()
    s3.Object(bucket, metaFile).delete()
    return initial_df
        

def put_doc(userId, timestamp, event_list):
# PUT /pinpoint-analysis/_doc/1
# {
#   "userId" : "u1",
#   "sessionList" : [
#     {
#       "session_time" : 12345,
#       "events": [
#         {"event_name": "e1", "time": 123},
#         {"event_name": "e2", "time": 124}
#       ]
#     }
#   ]
# }
    data = {"userId": userId, "sessionList": [{"sessionTime": timestamp, "eventList": event_list}]}
    json_document = json.dumps(data)
    while True:
        try:
            es.index(index=EStableName, doc_type= ESdocType, id=userId, body=json_document) 
            break
        except:
            print("put document error, sleep for 10s and try again")
            time.sleep(10)
       
    print("put new doc :", userId, " with timestamp ", timestamp)

def update_doc(userId, timestamp, event_list):
# POST /pinpoint-analysis/_update_by_query
# {
#   "script": {
#     "inline": "ctx._source.sessionList.add(params.new_session)",
#     "params": {
#       "new_session": 
#           {"session_time":22222,
#           "events":[{"event_name":"e3","time":233},
#           {"event_name":"e4","time":555}]}
#     }
#   },
#   "query": {
#     "terms": {
#         "_id": [
#           1
#         ]
#       }
#   }
# }
    upscript = {"script": {"inline": "ctx._source.sessionList.add(params.new_session)", "params": {"new_session":{"sessionTime": timestamp, "eventList": event_list}}}}
    while True:
        try:
            es.update(index=EStableName, doc_type=ESdocType, id=userId, body=upscript)
            break
        except:
            print("check existence error, sleep for 10s and try again")
            time.sleep(10)
    
    # data = {"script": {"inline": "ctx._source.sessionList.add(params.new_session)", "params": {"new_session":{"sessionTime": timestamp, "eventList": event_list}}}, "query": {"terms": {"_id":[userId]}}}
    # json_document = json.dumps(data)   
    # while True:
    #     response = requests.post(es_url, auth=awsauth, json=json_document, headers=ESheaders)
    #     status_code = response.status_code
    #     if (status_code < 400):
    #         break
    #     time.sleep(5)
    print("updated :", userId, " with timestamp ", timestamp)

def _decode(o):
    if isinstance(o, str):
        try:
            return int(o)
        except ValueError:
            return None
    elif isinstance(o, dict):
        return {k: _decode(v) for k, v in o.items()}
    elif isinstance(o, list):
        return [_decode(v) for v in o]
    else:
        return o

def parse_row(row):
    event_type_list = row['event_type_list'].replace("{}","").replace("null","")[1:-1].split(",")
    event_timestamp_list = row['event_timestamp_list'].replace("{}","")[1:-1].split(",")
    app_version_name_list = row['app_version_name_list'].replace("{}","").replace("null","")[1:-1].split(",")
    app_version_code_list = row['app_version_code_list'].replace("{}","").replace("null","")[1:-1].split(",")
    client_client_id_list = row['client_client_id_list'].replace("{}","").replace("null","")[1:-1].split(",")
    client_cognito_id_list = row['client_cognito_id_list'].replace("{}","").replace("null","")[1:-1].split(",")
    device_locale_code_list = row['device_locale_code_list'].replace("{}","").replace("null","")[1:-1].split(",")
    device_locale_country_list = row['device_locale_country_list'].replace("{}","").replace("null","")[1:-1].split(",")
    device_locale_language_list = row['device_locale_language_list'].replace("{}","").replace("null","")[1:-1].split(",")
    decice_make_list = row['decice_make_list'].replace("{}","").replace("null","")[1:-1].split(",")
    device_model_list = row['device_model_list'].replace("{}","").replace("null","")[1:-1].split(",")
    device_platform_name_list = row['device_platform_name_list'].replace("{}","").replace("null","")[1:-1].split(",")
    device_platform_version_list = row['device_platform_version_list'].replace("{}","").replace("null","")[1:-1].split(",")
    session_start_timestamp_list = row['session_start_timestamp_list'].replace("{}","")[1:-1].split(",")
    session_stop_timestamp_list = row['session_stop_timestamp_list'].replace("{}","")[1:-1].split(",")
    sessionId = row['sessionId']
    # attribute_list = [json.loads("{\"" + x.replace("=","\":\"").replace(", ","\",\"").replace("null","") + "\"}") for x in row['attribute_list'][2:-2].split("}, {")]
    attribute_list = []
    for x in row['attribute_list'].replace("[null,", "[{null},").replace(", null]", ", {null}]").replace(", null", ", {null}")[2:-2].split("}, {"):
        if x == 'null':
            attribute_list.append(None)
        else:
            try:
                attribute_list.append(json.loads("{\"" + x.replace("\n","").replace("\"","'").replace(":"," ").replace(" = ", " is ").replace("=","\":\"").replace(", ","\",\"").replace("null","") + "\"}"))
            except:
                print("attribute error with :\n", x)
                attribute_list.append(None)
    metrics_list = []
    for x in row['metrics_list'].replace("[null,", "[{null},").replace(", null]", ", {null}]").replace(", null", ", {null}")[2:-2].split("}, {"):
        if ((x == 'null') or (x == 'ul')):
            metrics_list.append(None)
        else:
            try:
                metrics_list.append(json.loads("{\"" + x.replace("=","\":\"").replace(", ","\",\"").replace("null","") + "\"}", object_hook=_decode))
            except:
                print(",etrics error with : \n", x)
                metrics_list.append(None)
    endpoint_channeltype_list = row['endpoint_channeltype_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_endpointstatus_list = row['endpoint_endpointstatus_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_optout_list = row['endpoint_optout_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_requestid_list = row['endpoint_requestid_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_location_country_list = row['endpoint_location_country_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_location_postalcode_list = row['endpoint_location_postalcode_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_location_city_list = row['endpoint_location_city_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_location_region_list = row['endpoint_location_region_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_demo_make_list = row['endpoint_demo_make_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_demo_model_list = row['endpoint_demo_model_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_demo_timezone_list = row['endpoint_demo_timezone_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_demo_locale_list = row['endpoint_demo_locale_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_demo_appversion_list = row['endpoint_demo_appversion_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_demo_platform_list = row['endpoint_demo_platform_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_demo_platformversion_list = row['endpoint_demo_platformversion_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_effectivedate_list = [x.replace(" ","").replace("T", " ").replace("Z","") for x in row['endpoint_effectivedate_list'].replace("{}","").replace("null","")[1:-1].split(",")]
    endpoint_attributes_list = row['endpoint_attributes_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_metrics_list = row['endpoint_metrics_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_applicationid_list = row['endpoint_applicationid_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_id_list = row['endpoint_id_list'.replace("{}","").replace("null","")][1:-1].split(",")
    endpoint_cohortid_list = row['endpoint_cohortid_list'].replace("{}","").replace("null","")[1:-1].split(",")
    endpoint_creationdate_list = [x.replace(" ","").replace("T", " ").replace("Z","") for x in row['endpoint_creationdate_list'].replace("{}","").replace("null","")[1:-1].split(",")]
    endpoint_user_userattributes_list = row['endpoint_user_userattributes_list'].replace("{}","").replace("null","")[1:-1].split(",")

    #add other attributes
    event_list = []
    for i in range(0,len(event_type_list)):
        event = {"event_type":event_type_list[i], "event_time_stamp": int(event_timestamp_list[i]) if 'null' not in event_timestamp_list[i] else None, "application": {"version_name": app_version_name_list[i], "version_code": app_version_code_list[i]}, "client": {"client_id": client_client_id_list[i], "cognito_id": client_cognito_id_list[i]}, "device":{"locale":{"code":device_locale_code_list[i], "country":device_locale_country_list[i], "language":device_locale_language_list[i]}, "make": decice_make_list[i], "model": device_model_list[i], "platform":{"name":device_platform_name_list[i], "version":device_platform_version_list[i]}}, "session":{"session_id": sessionId, "start_timestamp": int(session_start_timestamp_list[i]) if 'null'  not in session_start_timestamp_list[i] else None, "stop_timestamp":int(session_stop_timestamp_list[i]) if 'null' not in session_stop_timestamp_list[i] else None}, "attributes":attribute_list[i], "metrics":metrics_list[i], "endpoint":{"channeltype":endpoint_channeltype_list[i], "endpointstatus":endpoint_endpointstatus_list[i], "optout":endpoint_optout_list[i], "requestid":endpoint_requestid_list[i], "location":{"country":endpoint_location_country_list[i], "postalcode":endpoint_location_postalcode_list[i], "city":endpoint_location_city_list[i], "region":endpoint_location_region_list[i]}, "demographic":{"make":endpoint_demo_make_list[i], "model": endpoint_demo_model_list[i], "timezone":endpoint_demo_timezone_list[i], "locale":endpoint_demo_locale_list[i], "appversion":endpoint_demo_appversion_list[i], "platform":endpoint_demo_platform_list[i], "platformversion":endpoint_demo_platformversion_list[i]}, "effectivedate": endpoint_effectivedate_list[i], "attributes":endpoint_attributes_list[i], "metrics":endpoint_metrics_list[i], "applicationid":endpoint_applicationid_list[i], "id":endpoint_id_list[i], "cohortid":endpoint_cohortid_list[i], "creationdate":endpoint_creationdate_list[i], "user":{"userattributes":endpoint_user_userattributes_list[i]}}}
        event_list.append(event)
    return event_list
        
def write_to_ES(response):
    for index, row in response.iterrows():
        userId = row['userId']
        list_timestamp = row['list_timestamp']
        event_list = parse_row(row)    
        while True:
            try:
                existence = es.exists(EStableName,userId)
                break
            except:
                print("check existence error, sleep for 10s and try again")
                time.sleep(10)
        if existence:
            update_doc(userId, list_timestamp, event_list)
        else:
            put_doc(userId, list_timestamp, event_list)
        
    return True

def lambda_handler(event, context):
    response = query_athena()
    print("response finished")
    write_to_ES(response)  
    print("finished")

if __name__=="__main__":
    lambda_handler(None, None)